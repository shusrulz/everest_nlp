import os
java_path = "C:/Program Files/Java/jre1.8.0_291/bin/java.exe" 
os.environ['JAVA_HOME'] = java_path

import streamlit as st
from ner_devanagari import ner


def app():
    st.header("NER TAGGING")
    st.subheader("This app gives the ner tagging of the given input nepali text.")
    dner_obj = ner.DevanagriNER()
    #test_text = "जित बहादुर खाम्चा आहिले सानेपा ललितपुरमा रहेको इन्फोदेवेलोपेर्स प्राइभेट लिमिटेडमा काम गर्दै हुनुहुन्छ"
    #test_text = "नेपाल सरकारले  नेपालमा राम सिता दुबइ जन्मेको भन्छ"
    test_text = st.text_input("Please input your nepali sentence here:")
    if test_text:
        st.write("Your input text is: ", test_text)
        test_tokens = ner.tok.word_tokenize(test_text)
        tagged_tuples = dner_obj.tag_ner(test_tokens)
        #print(test_tokens)
        st.write("This is the list of tokens of input senetence", test_tokens)
        #print(tagged_tuples)
        st.write("This is the list of tagged tuples:", tagged_tuples)