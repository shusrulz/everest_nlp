import streamlit as st
from sabda2vec.inference import Sabda2Vec 


def app():
    st.header("Similarity")
    st.subheader("This app gives the similarity of words.")
    s2v_object = Sabda2Vec("sabda2vec_sm")
    test_word_1 = "सुसान्त"
    test_word_2 = "भालु"
    test_word_3 = "जित"
    st.write("The test words are:")
    st.write(test_word_1)
    st.write(test_word_2)
    st.write(test_word_3)
    #st.write("The test words are:", test_word_1, test_word_2, test_word_3)
    most_simialar = s2v_object.get_most_similar(test_word_2,5)
    #print("Top 10 words most similar to {} are \n {}".format(test_word_3,most_simialar))
    st.write("Top 10 words most similar to {} are \n {}:     " .format(test_word_3,most_simialar))
    sim = s2v_object.get_similarity_between_tokens(test_word_1,test_word_2)
    sim2 = s2v_object.get_similarity_between_tokens(test_word_2,test_word_3)
    print("sim",sim)
    st.write("sim",sim)
    print("sim2",sim2)
    st.write("sim2",sim2)
    # print("Similarity between {} and {} is {}".format(test_word_1,test_word_2,sim))
    # print("Similarity between {} and {}".format(test_word_1,test_word_3,sim2))
    # import pdb;pdb.set_trace()


