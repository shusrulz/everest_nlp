import streamlit as st
from sabda2vec.spell_checker import SpellChecker


def app():
    st.header("Spell Checker")
    st.subheader("This app Checks if the word is misspelled term and it gives a list of relevant suggestions.")
    spc = SpellChecker("sabda2vec_md")
    #text = 'रमण'
    text = st.text_input("Please input a Nepali word.")
    while text:
        st.write("Your  input is:   ", text)
        corrected_words = spc.correct_words(text)
        #corrected_words = '    ,  '.join(corrected_words)
        #print(corrected_words)
        Check_box = st.checkbox("Autocorrect", value=False, key=None)
        if Check_box:
            st.write("The list for autocorrect suggestions are:", corrected_words)
        break
