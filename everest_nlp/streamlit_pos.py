import streamlit as st
from pos_tagging.pos_inference import PosTest

def app():
    st.title("POS TAGGING")
    st.subheader("This app will give the pos tagging of the given user's input sentence.")
    #text = "म आजा भात खान्छु अनि बल्ल खेल्न जान्छु ।"
    text = st.text_input("Input a nepali sentence that you want to tag.")
    # text = "hey there mate"
    while text:
        model = PosTest("CRF_PoS_model.sav")
        result = model.getPos(text)
        #print (result)
        Check_box = st.checkbox("POS TAGGING", value=False, key=None)
        if Check_box:
            st.write("The pos tagging of the given words are as:", result)
        break
