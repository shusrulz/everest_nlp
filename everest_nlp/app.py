import streamlit_sim
import streamlit_spell
import streamlit_ner
import streamlit_pos
import streamlit as st

from multiapp import MultiApp

# PAGES = {
#     "App1": test,
#     "App2": test2
# }
# st.sidebar.title('Navigation')
# selection = st.sidebar.radio("Go to", list(PAGES.keys()))
# page = PAGES[selection]
# page.app()


app = MultiApp()
app.add_app("NER", streamlit_ner.app)
app.add_app("SpellChecker", streamlit_spell.app)
app.add_app("similarity", streamlit_sim.app)
app.add_app("Pos Tagging",streamlit_pos.app)

app.run()