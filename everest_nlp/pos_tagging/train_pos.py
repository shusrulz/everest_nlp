import os
import re
from glob import glob
import pickle
import sys

from sklearn_crfsuite import CRF
from sklearn_crfsuite.metrics import flat_classification_report,flat_f1_score,flat_accuracy_score,flat_recall_score,flat_precision_score
from sklearn.model_selection import train_test_split

class PosTrain():

    def __init__(self,data_path=None,algorithm="lbfgs",c1=0.01,c2=0.1,max_iterations=100,all_possible_transitions=True,verbose=True):
        self.model = CRF(
            algorithm=algorithm,
            c1=c1,
            c2=c2,
            max_iterations=max_iterations,
            all_possible_transitions=all_possible_transitions,
            verbose=verbose
        )
        if data_path is None:
            raise Exception("Give the path of your training data")
        sentences = self.readFiles(data_path)
        data_set = list(map(self.createToken,sentences))
        self.dataValidation(data_set)
        self.trainModel(data_set)

    def readFiles(self,data_path):
        # file_list = glob("pos_data/data_1/*.txt")
        try:
            file_list = glob(data_path)
            print (data_path)
        except Exception as e:
            print (e.__class__)
            print (e)
            sys.exit(1)
        
        sentences = []
        for item in file_list:
            with open(item,"r",encoding="UTF-8-SIG") as f:
                for line in f.readlines():
                    sentences.append(line)
        return sentences

    def createToken(self,sentence):
        tokens = re.split(r"<\w+><\w+>\s?|<\w+>\s?",sentence)[:-1]
        tags = re.findall(r"<\w+><\w+>|<\w+>",sentence)
        return [tokens,tags]

    def extractFeatures(self,sentence,index):
        return {
            'word':sentence[index],
            'is_first':index==0,
            'is_last':index ==len(sentence)-1,
            'is_alphanumeric': int(bool((re.match('^(?=.*[0-9]$)(?=.*)',sentence[index])))),
            'prefix-1':sentence[index][0],
            'prefix-2':sentence[index][:2],
            'prefix-3':sentence[index][:3],
            'prefix-4':sentence[index][:4],
            'suffix-1':sentence[index][-1],
            'suffix-2':sentence[index][-2:],
            'suffix-3':sentence[index][-3:],
            'suffix-4':sentence[index][-4:],
            'prev_word':'' if index == 0 else sentence[index-1],
            'next_word':'' if index < len(sentence) else sentence[index+1],
            'has_hyphen': '-' in sentence[index],
            'is_numeric': sentence[index].isdigit(),
        }

    def transformToDataset(self,tagged_sentences):
        X, y = [], []
        for sentence,tags in tagged_sentences:
            sent_word_features, sent_tags = [],[]
            for index in range(len(sentence)):
                sent_word_features.append(self.extractFeatures(sentence,index))
                sent_tags.append(tags[index])
            X.append(sent_word_features)
            y.append(sent_tags)
        return X,y

    def dataValidation(self,dataset):
        not_equal = []
        not_equal = [dataset.index(items) for items in dataset if len(items[0]) != len(items[1])]
        if not_equal:
            raise Exception(f"The Token and Tag are not equal. Make them equal inorder to train them for the PoS model. Changes are required in following indexes: \n{not_equal}")

    def trainModel(self,data_set):
        X_dataset,Y_dataset = self.transformToDataset(data_set)
        self.X_train,self.X_test,self.y_train,self.y_test = train_test_split(X_dataset,Y_dataset,test_size=0.2,random_state=111)
        self.model.fit(self.X_train,self.y_train)

    def testModel(self):
        pred_result = self.model.predict(self.X_test)
        print("Accuracy score on Test Data")
        print(flat_accuracy_score(self.y_test,pred_result,))
        print("**************")
        print("F1 score on Test Data")
        print(flat_f1_score(self.y_test,pred_result,average="weighted",labels=self.model.classes_))
        print("**************")
        print("Precision score on Test Data")
        print(flat_precision_score(self.y_test,pred_result,average="weighted",labels=self.model.classes_))
        print("**************")
        print("Recall score on Test Data")
        print(flat_recall_score(self.y_test,pred_result,average="weighted",labels=self.model.classes_))

    def showTags(self):
        try:
            tags = self.model.classes_
        except Exception as e:
            print (e.__class__)
            print (e)
            sys.exit(1)
        return tuple(tags)

    def saveModel(self,model_name):
        file_name = os.getcwd()+f"/pos_model/{model_name}"
        pickle.dump(self.model,open(file_name,'wb'))
        print (f"The model has been saved in the path {file_name}")


if __name__ == "__main__":
    file_path = os.getcwd()+"/pos_data/data_1/*.txt"
    pos_model = PosTrain(data_path=file_path)
    pos_model.testModel()
    pos_model.saveModel("CRF_PoS_model.sav")
    # tags = pos_model.showTags()
    # print (len(tags))
    # pos_model.trainModel(file_path)