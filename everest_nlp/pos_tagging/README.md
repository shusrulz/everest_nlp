## Training Custom PartofSpeech(PoS) tagging in Nepali Dataset
In this repositor, we will be training our custom PoS tagging model for Nepali langauge. CRF (Conditional Random Fields) algorithm has been used for training the model. The data that were used for training can be found in this link: - 
https://cle.org.pk/software/ling_resources/UrduNepaliEnglishParallelCorpus.htm

## Steps to train the Pos Model
Go inside the folder /pos_tagging and run the command in the terminal
- python3 train_pos.py
The trained model will be saved inside the folder /pos_tagging/pos_model/

## Steps to use the PoS Model
```python
from everest_nlp.pos_tagging.pos_inference import PosTest
pos_model = PosTest())
# Getting PoS of the given sentence 
text = "म आजा भात खान्छु अनि बल्ल खेल्न जान्छु ।"
result = pos_model.getPos(text)
# Getting the list of all the tags of the PoS model
tags = pos_model.showTags()
```