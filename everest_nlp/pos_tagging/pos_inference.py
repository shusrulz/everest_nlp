import os
import pickle
import re
import sys

from langdetect import detect

class PosTest():

    def __init__(self,model_name=None):
        if model_name is None:
            file_name = os.getcwd()+f"/pos_model/CRF_PoS_model.sav"
            self.model = pickle.load(open(file_name,'rb'))
        else:
            try:
                file_name = os.getcwd()+f"/pos_tagging/pos_model/{model_name}"
                self.model = pickle.load(open(file_name,'rb'))
            except Exception as e:
                print (e.__class__)
                print (e)
                sys.exit(1)

    def extractFeatures(self,sentence,index):
        return {
            'word':sentence[index],
            'is_first':index==0,
            'is_last':index ==len(sentence)-1,
            'is_alphanumeric': int(bool((re.match('^(?=.*[0-9]$)(?=.*)',sentence[index])))),
            'prefix-1':sentence[index][0],
            'prefix-2':sentence[index][:2],
            'prefix-3':sentence[index][:3],
            'prefix-4':sentence[index][:4],
            'suffix-1':sentence[index][-1],
            'suffix-2':sentence[index][-2:],
            'suffix-3':sentence[index][-3:],
            'suffix-4':sentence[index][-4:],
            'prev_word':'' if index == 0 else sentence[index-1],
            'next_word':'' if index < len(sentence) else sentence[index+1],
            'has_hyphen': '-' in sentence[index],
            'is_numeric': sentence[index].isdigit(),
        }

    def getTokens(self,sentence):
        return sentence.split(" ")

    def showTags(self):
        try:
            tags = self.model.classes_
        except Exception as e:
            print (e.__class__)
            print (e)
            sys.exit(1)
        return tuple(tags)

    def getPos(self,sentence):
        if detect(sentence) != "ne":
            raise Exception("Make sure the given sentence is only in Nepali langauge.")
        tokens = self.getTokens(sentence)
        features = [self.extractFeatures(tokens,index) for index in range(len(tokens))]
        result = self.model.predict_single(features)
        output = []
        for i in range(len(tokens)):
            output.append([tokens[i],result[i]])
            # print(tokens[i],"\t",result[i])
        return tuple(output)


if __name__ == "__main__":
    text = "म आजा भात खान्छु अनि बल्ल खेल्न जान्छु ।"
    # text = "hey there mate"
    model = PosTest("CRF_PoS_model.sav")
    result = model.getPos(text)
    print (result)